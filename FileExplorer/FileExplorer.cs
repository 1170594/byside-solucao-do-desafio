﻿using FileExplorer.Utils;
using Microsoft.VisualBasic;
using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace FileExplorer
{
    public partial class FileExplorer : Form
    {
        private ListViewItem dragSelected = null;
        private ListViewColumnSorter lvwColumnSorter = new ListViewColumnSorter();
        private bool isFile = false;
        private bool privateDrag;
        private bool deleted = false;
        private string currentFilePath = "C:/";
        private string currentSelectedItem = "";
        private string selectedFolder = "";
        private string copyItem = "";
        
        public FileExplorer()
        {
            InitializeComponent();
            Load_Setup();
        }
        private void Load_Setup()
        {
            OpenFileBrowser();
            listView1.ListViewItemSorter = lvwColumnSorter;
            this.WindowState = FormWindowState.Minimized;
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        public void loadFilesAndDirectories()
        {
            string tempFile = "";
            FileAttributes attr;

            if (isFile)
            {
                tempFile = path();
                FileInfo file = new FileInfo(tempFile);
                attr = File.GetAttributes(tempFile);
                Process.Start(tempFile);
            }
            else
            {
                attr = File.GetAttributes(currentFilePath);
            }
            if (DirectoryHandler.isDirectory(attr))
            {
                updateListView();
            }
        }
        public void updateListView()
        {
            listView1.Items.Clear();
            DirectoryHandler.setDirectories(listView1, currentFilePath);
            FileHandler.setDirectoryFiles(listView1, iconList, currentFilePath);
        }
        private void goButton_Click(object sender, EventArgs e)
        {
            OpenFileBrowser();
        }
        public void OpenFileBrowser()
        {
            using (FolderBrowserDialog fbd = new FolderBrowserDialog() { Description = "Select your path" })
            {
                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    filePath.Text = fbd.SelectedPath.Replace("\\", "/");
                    currentFilePath = fbd.SelectedPath.Replace("\\", "/");
                }
            }
            loadFilesAndDirectories();

        }
        public void loadButtonAction()
        {
            currentFilePath += selectedFolder;
            currentFilePath = currentFilePath.Replace("//", "/");
            loadFilesAndDirectories();
            isFile = false;
        }
        public void back()
        {
            try
            {
                string root = Directory.GetDirectoryRoot(currentFilePath).Replace("\\","");
                removeBackSlash();
                string path = filePath.Text;
                path = path.Substring(0, path.LastIndexOf("/"));
                isFile = false;
                if (path.Equals(root))
                    path += "/";
                filePath.Text = path;
                currentFilePath = path;
                selectedFolder = "";
                currentSelectedItem = "";
                loadFilesAndDirectories();
                removeBackSlash();

            }
            catch (Exception)
            {

            }
        }
        public void removeBackSlash()
        {
            string path = filePath.Text;
            if (path.LastIndexOf("/") == path.Length - 1)
            {
                filePath.Text = path.Substring(0, path.Length - 1);
            }
        }
        private void listView1_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            currentSelectedItem = e.Item.Text;
            if (deleted)
            {
                deleted = false;
            }
            else
            {
                FileAttributes attr = File.GetAttributes(path());

                if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                {
                    isFile = false;
                    selectedFolder = "/" + currentSelectedItem;
                }
                else
                {
                    selectedFolder = "";
                    isFile = true;
                }
            }

        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            loadButtonAction();
            filePath.Text = currentFilePath;
        }
        private string path()
        {
            return currentFilePath + "/" + currentSelectedItem;
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            back();
            loadButtonAction();
        }

        private void listView1_ItemDrag(object sender, ItemDragEventArgs e)
        {
            privateDrag = true;
            dragSelected = (ListViewItem)e.Item;
            DoDragDrop(e.Item, DragDropEffects.Move);
            privateDrag = false;
        }
        private void listView1_DragEnter(object sender, DragEventArgs e)
        {
            if (privateDrag) e.Effect = e.AllowedEffect;
        }
        private void listView1_DragOver(object sender, DragEventArgs e)
        {
            var point = listView1.PointToClient(new Point(e.X, e.Y));

            var item = listView1.HitTest(point).Item;
            if (item != null && (!item.Selected))
            {
                for (int i = 0; i < listView1.Items.Count; i++)
                {
                    listView1.Items[i].Selected = false;
                }
                item.Selected = true;
                listView1.EnsureVisible(item.Index);
            }
        }
        private void listView1_DragDrop(object sender, DragEventArgs e)
        {
            var point = listView1.PointToClient(new Point(e.X, e.Y));
            var item = listView1.GetItemAt(point.X, point.Y);
            if (item != null)
            {
                if (item.SubItems[2].Text.Equals("Folder") && !item.Equals(dragSelected))
                {
                    DirectoryHandler.MoveFolderOrFile(currentFilePath+"/"+ dragSelected.SubItems[0].Text, currentFilePath+"/"+ item.SubItems[0].Text + "/" + dragSelected.SubItems[0].Text);
                    listView1.Items.Remove(dragSelected);
                }
            }
        }

        private void menu_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                menu.Show(this, new Point(e.X, e.Y));
            }
        }

        private void copy_Click(object sender, EventArgs e)
        {
            menu.Items.Find("Paste", true)[0].Visible = true;
            copyItem = path();
            
        }

        private void paste_Click(object sender, EventArgs e)
        {
            
            if (FileHandler.isFile(copyItem))
            {
                FileHandler.copyFile(copyItem, currentFilePath+"/");
                updateListView();
            }
            else
            {
                DirectoryHandler.copyDirectory(copyItem,currentFilePath);
                updateListView();
            }
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string res = Interaction.InputBox("", "Add File", "", -1, -1);

            if (res.Length == 0)
            {
                return;
            }
            if (!FileHandler.ValidateFile(res))
            {
                MessageBox.Show("Invalid file name");
            }
            bool added = FileHandler.createFile(currentFilePath + "/" + res);
            if (added)
            {
                updateListView();
            }
        }

        private void folderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string res = Interaction.InputBox("", "Add folder", "", -1, -1);
            if (res.Length == 0)
            {
                return;
            }
            if (!DirectoryHandler.isValidPath(currentFilePath + "/" + res))
            {
                MessageBox.Show("Invalid folder name");
            }
            bool added = DirectoryHandler.createDirectory(currentFilePath+"/"+res);
            if (added)
            {
                updateListView();
            }
            
        }

        private void delete_Click(object sender, EventArgs e)
        {
            ListViewItem item = listView1.FindItemWithText(currentSelectedItem);

            if (item.SubItems[2].Text.Equals("Folder"))
            {
                DirectoryHandler.deleteDirectory(path());
                deleted = true;
            }
            else
            {
                FileHandler.deleteFile(path());
                deleted = true;
            }
            listView1.Items.Remove(item);


        }

        private void filePath_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (DirectoryHandler.isDirectory(filePath.Text))
                {
                    currentFilePath = filePath.Text;
                    loadFilesAndDirectories();
                }
                else if (FileHandler.isFile(filePath.Text))
                {
                    Process.Start(filePath.Text);
                    filePath.Text = currentFilePath;
                }
                else
                {
                    filePath.Text = currentFilePath;
                }
                e.Handled = true;
            }
        }

        private void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (e.Column == lvwColumnSorter.SortColumn)
            {
                if (lvwColumnSorter.Order == SortOrder.Ascending)
                {
                    lvwColumnSorter.Order = SortOrder.Descending;
                }
                else
                {
                    lvwColumnSorter.Order = SortOrder.Ascending;
                }
            }
            else
            {
                lvwColumnSorter.SortColumn = e.Column;
                lvwColumnSorter.Order = SortOrder.Ascending;
            }

            this.listView1.Sort();
        }

        private void propertiesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DirectoryHandler.isDirectory(path()) || FileHandler.isFile(path()))
            {
                DirectoryHandler.ShowFileProperties(path());
            }
        }
    }
}
/*
        public void loadTreeView()
        {
            //treeView1.Nodes.Clear();
            var rootC = new DirectoryInfo("C:\\");
            var rootD = new DirectoryInfo("D:");

            //treeView1.Nodes.Add(CreateNodeDirectory(rootC));
            //treeView1.Nodes.Add(CreateNodeDirectory(rootC));
        }
        private TreeNode CreateNodeDirectory(DirectoryInfo dir)
        {
            var dirNode = new TreeNode(dir.Name);
            
            foreach (var item in dir.GetDirectories())
            {
                if (DirectoryHandler.isAuthorized(item))
                {
                    dirNode.Nodes.Add(CreateNodeDirectory(item));
                }
            }
            foreach (var file in dir.GetFiles())
            {
                dirNode.Nodes.Add(new TreeNode(file.Name));
            }
            return dirNode;
        }
 */
