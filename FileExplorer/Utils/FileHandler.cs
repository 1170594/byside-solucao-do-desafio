﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileExplorer.Utils
{
    public class FileHandler
    {
        
        public static void setDirectoryFiles(ListView lstView1, ImageList iconList, string path)
        {
            DirectoryInfo dir = DirectoryHandler.FindDirectory(path);

            FileInfo[] files = GetAllFiles(dir);

            foreach (var item in files)
            {
                var index = 12;
                try
                {
                    index = fileExtensionIndex(item.Extension.Substring(1), iconList);
                }
                catch (Exception)
                {

                }
                //listView1.Items.Add(item.Name, index);
                ListViewItem lview = new ListViewItem();
                lview.ImageIndex = index;
                lview.Text = item.Name;

                lview.SubItems.Add("Date").Text = item.LastAccessTime.ToString();
                try
                {
                    lview.SubItems.Add("File").Text = item.Extension.Substring(1).ToUpper() + " File";
                }
                catch (Exception)
                {
                    lview.SubItems.Add("File").Text = item.Name + " File";
                }
                lview.SubItems.Add("Size").Text = item.Length.ToString();

                lstView1.Items.Add(lview);
            }
        }
        public static FileInfo[] GetAllFiles(DirectoryInfo dir)
        {
            try
            {
                return dir.GetFiles();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public static int fileExtensionIndex(string extension, ImageList iconList)
        {
            var key = extension + ".png";
            var index = iconList.Images.Keys.IndexOf(key);
            if (index == -1)
            {
                return 12;
            }
            return index;
        }
        public static bool ValidateFile(string fileName)
        {
            if (fileName.IndexOfAny(Path.GetInvalidFileNameChars()) != -1)
            {
                return false;
            }
            return true;
        }
        public static bool deleteFile(string path)
        {
            try
            {
                File.Delete(path);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
            return true;
        }
        public static bool isFile(string path)
        {
            return File.Exists(path);
        }
        public static bool createFile(string path)
        {
            try
            {
                if (File.Exists(path))
                {
                    MessageBox.Show("File already exists");
                    return false;
                }
                FileStream fileStream = File.Create(path);
                fileStream.Close();
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
        }
        public static bool copyFile(string fileToCopy, string destinationDirectory)
        {
            if (Path.GetDirectoryName(fileToCopy).Equals(Path.GetDirectoryName(destinationDirectory)))
            {
                string newFile = MakeUniqueFileName(fileToCopy);
                File.Copy(fileToCopy, destinationDirectory + newFile, true);
            }
            else
            {
                File.Copy(fileToCopy, destinationDirectory + Path.GetFileName(fileToCopy), true);
            }
            return true;

        }
        public static string MakeUniqueFileName(string path)
        {
            string dir = Path.GetDirectoryName(path);
            string fileName = Path.GetFileNameWithoutExtension(path);
            string fileExt = Path.GetExtension(path);

            for (int i = 1; ; ++i)
            {
                if (!File.Exists(path))
                    return Path.GetFileName(path);

                path = Path.Combine(dir, fileName + " Copy " + i + fileExt);
            }
        }
        
    }
}
