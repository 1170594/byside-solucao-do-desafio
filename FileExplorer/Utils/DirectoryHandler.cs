﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileExplorer.Utils
{
    public class DirectoryHandler
    {
        private const int SW_SHOW = 5;
        private const uint SEE_MASK_INVOKEIDLIST = 12;

        [DllImport("shell32.dll", CharSet = CharSet.Auto)]
        static extern bool ShellExecuteEx(ref SHELLEXECUTEINFO lpExecInfo);

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public struct SHELLEXECUTEINFO
        {
            public int cbSize;
            public uint fMask;
            public IntPtr hwnd;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpVerb;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpFile;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpParameters;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpDirectory;
            public int nShow;
            public IntPtr hInstApp;
            public IntPtr lpIDList;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpClass;
            public IntPtr hkeyClass;
            public uint dwHotKey;
            public IntPtr hIcon;
            public IntPtr hProcess;
        }
        public static bool isAuthorized(DirectoryInfo directory)
        {
            try
            {
                DirectorySecurity ds = Directory.GetAccessControl(directory.FullName);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static void setDirectories(ListView lstView1, string path)
        {
            DirectoryInfo dir = FindDirectory(path);
            DirectoryInfo[] dirs = GetAllDirectories(dir);

            if (dirs == null)
                return;

            foreach (var item in dirs)
            {
                if (isAuthorized(item))
                {
                    //listView1.Items.Add(item.Name, 16);
                    ListViewItem lview = new ListViewItem();
                    lview.ImageIndex = 16;
                    lview.Text = item.Name;

                    lview.SubItems.Add("Date").Text = item.LastAccessTime.ToString();
                    lview.SubItems.Add("File").Text = "Folder";
                    lview.SubItems.Add("Size").Text = "";
                    lstView1.Items.Add(lview);
                }
            }
        }
        public static DirectoryInfo FindDirectory(string path)
        {
            try
            {
                return new DirectoryInfo(path);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public static DirectoryInfo[] GetAllDirectories(DirectoryInfo dir)
        {
            try
            {
                return dir.GetDirectories();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public static bool isDirectory(FileAttributes attr)
        {
            return (attr & FileAttributes.Directory) == FileAttributes.Directory;
        }
        public static bool isDirectory(string dir)
        {
            return Directory.Exists(dir);
        }
        public static bool isValidPath(string path)
        {
            if (path.IndexOfAny(Path.GetInvalidPathChars()) != -1)
            {
                return false;
            }
            return true;
        }
        public static bool deleteDirectory(string path)
        {
            try
            {
                Directory.Delete(path, true);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
        public static bool directoryExists(string path)
        {
            return Directory.Exists(path);
        }
        public static bool createDirectory(string path)
        {
            try
            {
                if (Directory.Exists(path))
                {
                    MessageBox.Show("Folder name already exists");
                    return false;
                }
                Directory.CreateDirectory(path);
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
        }
        public static bool copyDirectory(string sourceDirectory, string targetDirectory)
        {
            string x = targetDirectory + sourceDirectory.Substring(sourceDirectory.LastIndexOf("/"));
            x = MakeFolderUnique(x);
            Directory.CreateDirectory(x);
            DirectoryInfo source = new DirectoryInfo(sourceDirectory);
            DirectoryInfo target = new DirectoryInfo(x);
            CopyAll(source,target);
            //FileSystem.CopyDirectory(x, targetDirectory);
            return true;
        }

        public static void CopyAll(DirectoryInfo source, DirectoryInfo target)
        {
            Directory.CreateDirectory(target.FullName);

            foreach (FileInfo fi in source.GetFiles())
            {
                fi.CopyTo(Path.Combine(target.FullName, fi.Name), true);
            }

            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir =
                    target.CreateSubdirectory(diSourceSubDir.Name);
                CopyAll(diSourceSubDir, nextTargetSubDir);
            }
        }
        public static string MakeFolderUnique(string dir)
        {
            string temp = dir;
            int index = 0;
            while (Directory.Exists(dir))
                dir = temp + " (" + (++index) + ")";
            return dir;
        }
        public static void MoveFolderOrFile(string source,string destination)
        {
            Directory.Move(source,destination);
        }
        public static bool ShowFileProperties(string Filename)
        {
            SHELLEXECUTEINFO info = new SHELLEXECUTEINFO();
            info.cbSize = Marshal.SizeOf(info);
            info.lpVerb = "properties";
            info.lpFile = Filename;
            info.lpParameters = "Security";
            info.nShow = SW_SHOW;
            info.fMask = SEE_MASK_INVOKEIDLIST;
            return ShellExecuteEx(ref info);
        }
    }

}
